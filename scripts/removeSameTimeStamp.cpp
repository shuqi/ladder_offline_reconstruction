void remove(TString RunNumber, TString BoardNum, TString treeName1, TString treeName2, TString treeName1Out, TString treeName2Out){

  TFile *inFile = new TFile("/home/dell/ladder_Offline_Reconstruction/ladder_DESY_data/"+RunNumber+"/Outputhit_2_9_Board"+BoardNum+".root", "read");

  TTree *t = (TTree*)inFile->Get(treeName1);

  int inchipID, inrow, incol, intimeFPGA, intimeChip;
  t->SetBranchAddress("chipID", &inchipID);
  t->SetBranchAddress("row", &inrow);
  t->SetBranchAddress("col", &incol);
  t->SetBranchAddress("timeFPGA", &intimeFPGA);
  t->SetBranchAddress("timeChip", &intimeChip);

  TTree *t2 = (TTree*)inFile->Get(treeName2);

  int inchipID2, inrow2, incol2, intimeFPGA2, intimeChip2;
  t2->SetBranchAddress("chipID", &inchipID2);
  t2->SetBranchAddress("row", &inrow2);
  t2->SetBranchAddress("col", &incol2);
  t2->SetBranchAddress("timeFPGA", &intimeFPGA2);
  t2->SetBranchAddress("timeChip", &intimeChip2);


  TFile *outFile = new TFile("/home/dell/ladder_Offline_Reconstruction/ladder_DESY_data/"+RunNumber+"/Outputhit_2_9_Board"+BoardNum+"_remove2.root", "RECREATE");
  TTree* myTree = new TTree(treeName1Out, "");
  int chipID, row, col, timeFPGA, timeChip;
  myTree->Branch("chipID", &chipID);
  myTree->Branch("row", &row);
  myTree->Branch("col", &col);
  myTree->Branch("timeFPGA", &timeFPGA);
  myTree->Branch("timeChip", &timeChip);
  
  if (BoardNum == "04") treeName2Out = "HitsInfo_4"; 
  TTree* myTree2 = new TTree(treeName2Out, "");
  int chipID2, row2, col2, timeFPGA2, timeChip2;
  myTree2->Branch("chipID", &chipID2);
  myTree2->Branch("row", &row2);
  myTree2->Branch("col", &col2);
  myTree2->Branch("timeFPGA", &timeFPGA2);
  myTree2->Branch("timeChip", &timeChip2);


  int prerow = -1;
  int precol = -1;
  int pretimeFPGA = -1;
  int pretimeChip = -1;
  bool ifrepeat;
  int tag1 = 0;
  for (int i = 0; i < (int)t->GetEntries(); i++)
  {
    t->GetEntry(i);
    if (i%1000000 == 0) std::cout << "i = " << i << std::endl;
    if (intimeFPGA == 0) continue;
    if (i < 200 && intimeFPGA > 100000) continue;
    // if (intimeFPGA == 0 || intimeFPGA < 200) continue;
    // if (intimeFPGA == 33556720) continue;
    if (tag1 == 1)
    {
      pretimeFPGA = timeFPGA;
      pretimeChip = timeChip;
      precol = col;
      prerow = row;
    }
    ifrepeat = (prerow == inrow && precol == incol && pretimeFPGA == intimeFPGA && pretimeChip == intimeChip);

    chipID = inchipID;
    row = inrow;
    col = incol;
    timeFPGA = intimeFPGA;
    timeChip = intimeChip;
    if (ifrepeat) continue;
    int diff1 = pretimeFPGA - intimeFPGA; 
    if ((pretimeFPGA > intimeFPGA)) 
    {
      tag1 = 0;
    }
    else
    {
      if (tag1 != 0 && BoardNum != "04") myTree->Fill();
      tag1 = 1;
    }

    /*
    if (pretimeFPGA > intimeFPGA)
    {
      std::cout << "i = " << i << std::endl;
      std::cout << "pretimeFPGA > intimeFPGA" << std::endl;
      std::cout << "pretimeFPGA = " << pretimeFPGA << std::endl;
      std::cout << "intimeFPGA = " << intimeFPGA << std::endl;
      std::cout << "diff = " << pretimeFPGA - intimeFPGA << std::endl;
      continue;
    }
    */
  }
  if (BoardNum != "04") myTree->Write();


  int prerow2 = -1;
  int precol2 = -1;
  int pretimeFPGA2 = -1;
  int pretimeChip2 = -1;
  bool ifrepeat2;
  int tag2 = 0;
  for (int i = 0; i < (int)t2->GetEntries(); i++)
  {
    t2->GetEntry(i);
    if (i%1000000 == 0) std::cout << "i = " << i << std::endl;
    if (intimeFPGA2 == 0) continue;
    if (i < 200 && intimeFPGA2 > 100000) continue;

    //if (intimeFPGA2 == 0 || intimeFPGA2 < 200) continue;
    if (tag2 == 1)
    {
      pretimeFPGA2 = timeFPGA2;
      pretimeChip2 = timeChip2;
      precol2 = col2;
      prerow2 = row2;
    }
    ifrepeat2 = (prerow2 == inrow2 && precol2 == incol2 && pretimeFPGA2 == intimeFPGA2 && pretimeChip2 == intimeChip2);
    //std::cout << "pretimeFPGA = " << pretimeFPGA << std::endl;
    //std::cout << "intimeFPGA = " << timeFPGA << std::endl;
    //std::cout << "aaaaa" << std::endl;

    chipID2 = inchipID2;
    row2 = inrow2;
    col2 = incol2;
    timeFPGA2 = intimeFPGA2;
    timeChip2 = intimeChip2;
    if (ifrepeat2) continue;
    int diff2 = pretimeFPGA2 - intimeFPGA2;
    if ((pretimeFPGA2 > intimeFPGA2))
    {
      /*
      std::cout << "bbbbb" << std::endl;
      std::cout << "i = " << i << std::endl;
      std::cout << "pretimeFPGA2 > intimeFPGA2" << std::endl;
      std::cout << "pretimeFPGA2 = " << pretimeFPGA2 << std::endl;
      std::cout << "intimeFPGA2 = " << intimeFPGA2 << std::endl;
      std::cout << "diff2 = " << pretimeFPGA2 - intimeFPGA2 << std::endl;
      */
      tag2 = 0;
    }
    else
    {
      if (tag2 != 0) myTree2->Fill();
      tag2 = 1;
    }
  }
  myTree2->Write();
  inFile->Close();
  outFile->Close();
}

void removeSameTimeStamp(){
  TString RunNumber = "Run0638";
  TString BoardNum;
  TString treeName1, treeName2, treeName1Out, treeName2Out;
  int numth, numthOut;
  for (int i = 1; i < 7; i++){
    
    if (i == 1) {numth = 10; numthOut = 9;}
    else if (i == 2) {numth = 8; numthOut = 7;}
    else if (i == 3) {numth = 6; numthOut = 5;}
    else if (i == 4) {numth = 4; numthOut = 4;}
    else if (i == 5) {numth = 2; numthOut = 2;}
    else if (i == 6) {numth = 0; numthOut = 0;}

    treeName1 = "HitsInfo_"+to_string(numth);
    treeName2 = "HitsInfo_"+to_string(numth+1);

    treeName1Out = "HitsInfo_"+to_string(numthOut);
    treeName2Out = "HitsInfo_"+to_string(numthOut+1); 
    BoardNum = "0"+std::to_string(i);
    std::cout << "BoardNum = " << BoardNum << std::endl;
    remove(RunNumber, BoardNum, treeName1, treeName2, treeName1Out, treeName2Out);
  }
}
