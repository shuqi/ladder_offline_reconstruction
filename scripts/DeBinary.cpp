#include<iostream>
#include<fstream>
#include<vector>
using namespace std;


void DoReading(TString RunNumber, TString BoardNum, int NumOfChips)
{
  TString fileTag = "01";
  TString directory = "/home/dell/ladder_Offline_Reconstruction/ladder_DESY_data/";
  TFile* OutputFile = new TFile(directory+RunNumber+"/Outputhit_2_9_Board"+BoardNum+"_"+fileTag+".root", "RECREATE");
  TDirectory* dir[NumOfChips];
  TTree* myTree[NumOfChips];
  // TTree* myTree;
  int valid     = -1;
  int begFlag   = -1;
  int chipID    = 0;
  int dataCount = 0;
  int curID     = 0;
  int preID     = 0;
  int passVal   = 0;
  int row       = 0;
  double newrow = 0;
  double newcol = 0;
  int col       = 0;
  int timeFPGA  = 0;
  int timeChip  = 0;

  int prerow       = 0;
  int precol       = 0;
  int pretimeFPGA  = 0;
  int pretimeChip  = 0;
  int prechipID    = 0;

  int hitNumber = 0;  // in every packet
  bool IsTrailer = false;
  int num = 0;

  for (int i = 0; i < NumOfChips; i++)
  {
    int numth;
    if (BoardNum == "01") numth = 10 + i;
    else if (BoardNum == "02") numth = 8 + i;
    else if (BoardNum == "03") numth = 6 + i;
    else if (BoardNum == "04") numth = 4 + i;
    else if (BoardNum == "05") numth = 2 + i;
    else if (BoardNum == "06") numth = 0 + i;


    TString treeName = "HitsInfo_"+to_string(numth);
    // dir[i] = OutputFile->mkdir(dirName);
    // dir[i]->cd();

    myTree[i] = new TTree(treeName, "test beam data");
    // myTree[i] = new TTree("HitsInfo", "test beam data");
    myTree[i]->Branch("PacketID", &curID);
    myTree[i]->Branch("PacketDataNum", &dataCount);
    myTree[i]->Branch("HitsNum", &hitNumber);
    myTree[i]->Branch("valid", &valid);
    myTree[i]->Branch("chipID", &chipID);
    myTree[i]->Branch("timeFPGA", &timeFPGA);
    myTree[i]->Branch("timeChip", &timeChip);
    myTree[i]->Branch("row", &row);
    myTree[i]->Branch("col", &col);
    // myTree[i]->Write();  
  }

  size_t length;
  ifstream inF;
  
  inF.open(directory+RunNumber+"/"+RunNumber+"-RunData-Board"+BoardNum+"/"+"Board-"+BoardNum+"(00"+fileTag+").bin", std::ifstream::binary);
  inF.seekg(0, ios::end);
  length = inF.tellg();
  cout << "the length of the file is " << length << " " << "byte" << endl;
  
  length = 8;
  unsigned char* data = new unsigned char[length]();

  inF.clear();
  inF.seekg(3, ios::beg);
  cout << "the position of the beginner " << inF.tellg() << endl;
  inF.get();
  if (!inF.eof()) {
    cout << "target reading..." << endl;
    while(inF.read(reinterpret_cast<char*>(data), sizeof(char)* length))
    {
      num++;
      if (num%500000 == 0) std::cout << "processing " << num << " packet..." << std::endl; 
      // if (num == 16) break;
      unsigned long long int* data0 = new unsigned long long int[length];
      // for (size_t i = 0; i < length; i++){
      //   data0[i] = data[i];
      //   cout << setw(2) << setfill('0') << setiosflags(ios::uppercase) << hex << data0[i]<<" ";
      // }
      // cout << "" << endl;
      // cout << "the position of the current pointor " << dec <<inF.tellg() << endl;
      if (data[0] == 0xaa && data[1] == 0xbb){
        hitNumber = 0;
        // cout << dec << "Reading one packet..." << endl;
        dataCount = data[2] & 0xff;
        preID = curID;
        curID = (data[4] << 24 & 0xffffffff) | (data[5] << 16 & 0xffffff) | (data[6] << 8 & 0xffff) | (data[7] & 0xff);
        int testID = preID + 1;
        if (curID - preID != 1) cout << "packet header ID error, Current ID should be " << dec << testID << ", rather than " << curID << endl;
        while (inF.read(reinterpret_cast<char*>(data), sizeof(char)* length))
        {
          precol = col;
          prerow = row;
          pretimeChip = timeChip;
          pretimeFPGA = timeFPGA;
          prechipID = chipID;

          valid  = data[7] & 0x01;
          chipID = data[0] >> 4 & 0x0f;
          timeFPGA = (data[1] << 24 & 0xfffffff) | (data[2] << 16 & 0xffffff) | (data[3] << 8 & 0xffff) | (data[4] & 0xff);
          timeChip = (data[0] << 4 & 0xf0) | (data[1] >> 4 & 0x0f);
          row      = (data[6] << 3 & 0x3ff) | (data[7] >>5 & 0x07);
          col      = (data[5] << 1 & 0x1ff) | (data[6] >> 7 & 0x01);
          if (precol == col && prerow == row && pretimeChip == timeChip && pretimeFPGA == timeFPGA && prechipID == chipID) 
	        {
	          //std::cout << "same !!!" << std::endl;
            continue;
	        }

          newrow = row/2;
          if ((row%4 == 0) || (row%4 == 3)) { newcol = col*2;}
          else {newcol = col*2 + 1;}
          
          if ( data[0] == 0xbb && data[1] == 0xaa ) break;
          hitNumber ++;
          // std::cout << "PacketID = " << curID << std::endl;
          // std::cout << "PacketDataNum = " << dataCount << std::endl;
          // std::cout << "HitsNum = " << hitNumber << std::endl;
          // std::cout << "chipID = " << chipID << std::endl;
          // std::cout << "timeFPGA = " << timeFPGA << std::endl;
          // std::cout << "timeChip = " << timeChip << std::endl;
          // std::cout << "valid = " << valid << std::endl;
          // std::cout << "row = " << row << std::endl;
          // std::cout << "col = " << col << std::endl;
          int saveID1, saveID2;
          if (BoardNum == "06" || BoardNum == "05" || BoardNum == "04")
          {
            //saveID1 = 10;
            //saveID2 = 1;
            saveID1 = 9;
	    saveID2 = 2;
	  }
          else
          {
            //saveID1 = 1;
            //saveID2 = 10;
            saveID1 = 2;
	    saveID2 = 9;
	  }
          if(chipID == saveID1) myTree[0]->Fill();
          if(chipID == saveID2) myTree[1]->Fill();
          // if(chipID == 9) myTree[2]->Fill();
          // if(chipID == 10) myTree[3]->Fill();
        }
      }
      else if ( data[0] == 0xbb && data[1] == 0xaa ) std::cout << "No packet header, trailer only ..." << std::endl;
    }
  }
  else { std::cout << "No found input files..." << std::endl; }
  inF.close();
  for (int i = 0; i < NumOfChips; i++)
  {
    myTree[i]->Write();  
  }

  // myTree->Write();  
  // OutputFile->Close();
  delete[] data;
}

void DeBinary()
{
  int NumOfChips = 2;
  TString RunNumber = "Run0638";
  TString BoardNum;
  for (int i = 6; i < 7; i++){
    BoardNum = "0"+std::to_string(i);
    DoReading(RunNumber, BoardNum, NumOfChips);
  }
}
