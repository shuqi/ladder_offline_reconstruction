void remove(TString RunNumber, TString BoardNum, TString treeName1, TString treeName2){

  TFile *inFile = new TFile("/home/dell/ladder_Offline_Reconstruction/ladder_DESY_data/"+RunNumber+"/Outputhit_2_9_Board"+BoardNum+".root", "read");


  TTree *t = (TTree*)inFile->Get(treeName1);

  int inchipID, inrow, incol, intimeFPGA, intimeChip;
  t->SetBranchAddress("chipID", &inchipID);
  t->SetBranchAddress("row", &inrow);
  t->SetBranchAddress("col", &incol);
  t->SetBranchAddress("timeFPGA", &intimeFPGA);
  t->SetBranchAddress("timeChip", &intimeChip);

  TTree *t2 = (TTree*)inFile->Get(treeName2);

  int inchipID2, inrow2, incol2, intimeFPGA2, intimeChip2;
  t2->SetBranchAddress("chipID", &inchipID2);
  t2->SetBranchAddress("row", &inrow2);
  t2->SetBranchAddress("col", &incol2);
  t2->SetBranchAddress("timeFPGA", &intimeFPGA2);
  t2->SetBranchAddress("timeChip", &intimeChip2);


  TFile *outFile = new TFile("/home/dell/ladder_Offline_Reconstruction/ladder_DESY_data/"+RunNumber+"/Outputhit_2_9_Board"+BoardNum+"_remove1.root", "RECREATE");
  TTree* myTree = new TTree(treeName1, "");
  int chipID, row, col, timeFPGA, timeChip;
  myTree->Branch("chipID", &chipID);
  myTree->Branch("row", &row);
  myTree->Branch("col", &col);
  myTree->Branch("timeFPGA", &timeFPGA);
  myTree->Branch("timeChip", &timeChip);

  TTree* myTree2 = new TTree(treeName2, "");
  int chipID2, row2, col2, timeFPGA2, timeChip2;
  myTree2->Branch("chipID", &chipID2);
  myTree2->Branch("row", &row2);
  myTree2->Branch("col", &col2);
  myTree2->Branch("timeFPGA", &timeFPGA2);
  myTree2->Branch("timeChip", &timeChip2);


  int prerow = -1;
  int precol = -1;
  int pretimeFPGA = -1;
  int pretimeChip = -1;
  bool ifrepeat;
  int tag1 = 0;
  for (int i = 0; i < (int)t->GetEntries(); i++)
  {
    t->GetEntry(i);
    if (i%1000000 == 0) std::cout << "i = " << i << std::endl;
    // if (i < 500) break;
    if (intimeFPGA == 0) continue;
    if (i < 500 && intimeFPGA > 1000000) continue;
    pretimeFPGA = timeFPGA;
    pretimeChip = timeChip;
    precol = col;
    prerow = row;
    
    ifrepeat = (prerow == inrow && precol == incol && pretimeFPGA == intimeFPGA && pretimeChip == intimeChip);

    chipID = inchipID;
    row = inrow;
    col = incol;
    timeFPGA = intimeFPGA;
    timeChip = intimeChip;

    
    if (ifrepeat)
    {
      continue;
    }

    myTree->Fill();
    
  }
  myTree->Write();


  int prerow2 = -1;
  int precol2 = -1;
  int pretimeFPGA2 = -1;
  int pretimeChip2 = -1;
  bool ifrepeat2;
  int tag2 = 0;
  for (int i = 0; i < (int)t2->GetEntries(); i++)
  {
    t2->GetEntry(i);
    if (i%1000000 == 0) std::cout << "i = " << i << std::endl;

    if (intimeFPGA2 == 0) continue;
    if (i < 500 && intimeFPGA2 > 1000000) continue;

    pretimeFPGA2 = timeFPGA2;
    pretimeChip2 = timeChip2;
    precol2 = col2;
    prerow2 = row2;

    ifrepeat2 = (prerow2 == inrow2 && precol2 == incol2 && pretimeFPGA2 == intimeFPGA2 && pretimeChip2 == intimeChip2);
    //std::cout << "pretimeFPGA = " << pretimeFPGA << std::endl;
    //std::cout << "intimeFPGA = " << timeFPGA << std::endl;
    //std::cout << "aaaaa" << std::endl;

    chipID2 = inchipID2;
    row2 = inrow2;
    col2 = incol2;
    timeFPGA2 = intimeFPGA2;
    timeChip2 = intimeChip2;

    if (ifrepeat2)
    {
      continue;
    }
    myTree2->Fill();
  }
  myTree2->Write();
  inFile->Close();
  outFile->Close();
}

void removeSame(){
  TString RunNumber = "Run0605";
  TString BoardNum;
  TString treeName1, treeName2;
  int numth;
  for (int i = 1; i < 7; i++){
    //if (i == 4) continue;
    
    if (i == 1) numth = 10;
    else if (i == 2) numth = 8;
    else if (i == 3) numth = 6;
    else if (i == 4) numth = 4;
    else if (i == 5) numth = 2;
    else if (i == 6) numth = 0;

    treeName1 = "HitsInfo_"+to_string(numth);
    treeName2 = "HitsInfo_"+to_string(numth+1);
    BoardNum = "0"+std::to_string(i);
    std::cout << "BoardNum = " << BoardNum << std::endl;
    remove(RunNumber, BoardNum, treeName1, treeName2);
  }
}
