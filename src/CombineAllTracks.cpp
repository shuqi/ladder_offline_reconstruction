#include <iostream>
#include <vector>
#include <algorithm>

#include "IncludeFun.h"
#include "CombineAllTracks.h"

using namespace std;

CombineAllTracks::CombineAllTracks()
{
  std::cout << "************************************" << std::endl;
  std::cout << "*** combining all possible tracks***"  << std::endl;
  std::cout << "************************************" << std::endl;
}

CombineAllTracks::~CombineAllTracks()
{

}

void CombineAllTracks::CombineCluster(TString directory, TString RunNumber, TString hitOption){
    TFile *InFile = new TFile(directory+RunNumber+"/Clusterxy_"+hitOption+"2_9.root", "read");
    TTree *tInFile = (TTree*)InFile->Get("Event");

    int EventNum;
    tInFile->SetBranchAddress("EventNum", &EventNum);
    int TotalClusters;
    tInFile->SetBranchAddress("TotClusterNum", &TotalClusters);
    vector<int>* vec_plane           = new std::vector<int>();
    tInFile->SetBranchAddress("planeID", &vec_plane);
    vector<int>* vec_ClusterNum      = new std::vector<int>();
    tInFile->SetBranchAddress("clusterNum", &vec_ClusterNum);
    vector<double>* vec_col          = new std::vector<double>();
    tInFile->SetBranchAddress("col", &vec_col);
    vector<double>* vec_row          = new std::vector<double>();
    tInFile->SetBranchAddress("row", &vec_row);
    std::vector<int>* vec_clusterID      = new std::vector<int>();
    tInFile->SetBranchAddress("clusterID", &vec_clusterID);
    vector<int>* vec_hitsNum         = new std::vector<int>();
    tInFile->SetBranchAddress("hitsNum", &vec_hitsNum);
    std::vector<int>* vec_hitsNumX      = new std::vector<int>(); 
    tInFile->SetBranchAddress("hitsNumX", &vec_hitsNumX);
    std::vector<int>* vec_hitsNumY      = new std::vector<int>();
    tInFile->SetBranchAddress("hitsNumY", &vec_hitsNumY);
    vector<double>* vec_xloc         = new std::vector<double>();
    tInFile->SetBranchAddress("xloc", &vec_xloc);
    vector<double>* vec_yloc         = new std::vector<double>();
    tInFile->SetBranchAddress("yloc", &vec_yloc);
    vector<double>* vec_xglo         = new std::vector<double>();
    tInFile->SetBranchAddress("xglo", &vec_xglo);
    vector<double>* vec_yglo         = new std::vector<double>();
    tInFile->SetBranchAddress("yglo", &vec_yglo);
    vector<double>* vec_zglo         = new std::vector<double>();
    tInFile->SetBranchAddress("zglo", &vec_zglo);

    TFile *OutFile = new TFile(directory+RunNumber+"/"+RunNumber+"_AllTrackxy_"+hitOption+"2_9_all.root", "RECREATE");
    // Out Tree
    TTree* myTree = new TTree("TrackInfo", "Roughly selected Tracks");

    int EventNumOut;
    myTree->Branch("EventNum", &EventNumOut);

    int TrackSize;
    myTree->Branch("TrackSize", &TrackSize);

    std::vector<int>* vec_planeOut        = new std::vector<int>();
    myTree->Branch("planeID", &vec_planeOut);
    std::vector<double>* vec_colOut       = new std::vector<double>();
    myTree->Branch("col", &vec_colOut);
    std::vector<double>* vec_rowOut       = new std::vector<double>();
    myTree->Branch("row", &vec_rowOut);
    std::vector<double>* vec_xlocOut      = new std::vector<double>();
    myTree->Branch("xloc", &vec_xlocOut);
    std::vector<double>* vec_ylocOut      = new std::vector<double>();
    myTree->Branch("yloc", &vec_ylocOut);
    std::vector<double>* vec_xgloOut      = new std::vector<double>();
    myTree->Branch("xglo", &vec_xgloOut);
    std::vector<double>* vec_ygloOut      = new std::vector<double>();
    myTree->Branch("yglo", &vec_ygloOut);
    std::vector<double>* vec_zgloOut      = new std::vector<double>();
    myTree->Branch("zglo", &vec_zgloOut);
    
    std::vector<int>* vec_clusterIDOut      = new std::vector<int>();
    myTree->Branch("clusterID", &vec_clusterIDOut);

    vector<int>* vec_hitsNumOut         = new std::vector<int>();
    myTree->Branch("hitsNum", &vec_hitsNumOut);
    std::vector<int>* vec_hitsNumXOut      = new std::vector<int>(); 
    myTree->Branch("hitsNumX", &vec_hitsNumXOut);
    std::vector<int>* vec_hitsNumYOut      = new std::vector<int>();
    myTree->Branch("hitsNumY", &vec_hitsNumYOut);

    int first_dimension;
    // int result[][];
    for (int m = 0; m < (int)tInFile->GetEntries(); m++){
      tInFile->GetEntry(m);
      if (m%10000 == 0) std::cout << "Processing " << m << "..." << std::endl;
      // std::cout << "m = " << m << std::endl;
      // if (TotalClusters > 50) continue;
      
      EventNumOut = EventNum;
      vec_rowOut->clear();
      vec_colOut->clear();
      vec_planeOut->clear();
      vec_xlocOut->clear();
      vec_ylocOut->clear();
      vec_xgloOut->clear();
      vec_ygloOut->clear();
      vec_zgloOut->clear();
      vec_hitsNumOut->clear();
      vec_hitsNumXOut->clear();
      vec_hitsNumYOut->clear();
      vec_clusterIDOut->clear();
      first_dimension = 1;
      vector<vector<int>> Selplane;
      for (int i = 0; i < NumOfChips; i++)
      {
        Selplane.push_back(findVectors(vec_plane, i));
        first_dimension *= Selplane[i].size();
      }
      // if (first_dimension > 800) continue;
      TrackSize = first_dimension;
      int result[TrackSize][11]; // !!!!!!!!!!!!!!!!!
      get_all_combination(Selplane, result);

      for (int i = 0; i < first_dimension; i++)
      {
        // std::cout << "i = " << i << std::endl;
        for (int j = 0; j < NumOfChips; j++)
        {
          // cout<<result[i][j]<<"\t";
          // std::cout << "j = " << j << std::endl;
          int Selposition = result[i][j];
          vec_planeOut->push_back(j);
          vec_rowOut->push_back(vec_row->at(Selposition));
          vec_colOut->push_back(vec_col->at(Selposition));
          vec_clusterIDOut->push_back(vec_clusterID->at(Selposition));
          vec_xlocOut->push_back(vec_xloc->at(Selposition));
          vec_ylocOut->push_back(vec_yloc->at(Selposition));
          vec_xgloOut->push_back(vec_xglo->at(Selposition));
          vec_ygloOut->push_back(vec_yglo->at(Selposition));
          vec_zgloOut->push_back(vec_zglo->at(Selposition));
          vec_hitsNumOut->push_back(vec_hitsNum->at(Selposition));
          vec_hitsNumXOut->push_back(vec_hitsNumX->at(Selposition));
          vec_hitsNumYOut->push_back(vec_hitsNumY->at(Selposition));
        }
        // cout<<endl;
      }
      myTree->Fill();
  }
  myTree->Write();
  InFile->Close();
  OutFile->Close();
}