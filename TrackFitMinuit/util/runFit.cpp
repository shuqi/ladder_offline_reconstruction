#include "TrackFit.h"

int main(int argc, char **argv)
{
  TString RunNum = argv[1];
  TFile* InputFile = new TFile("/home/dell/ladder_Offline_Reconstruction/ladder_DESY_data/"+RunNum+"/"+RunNum+"_AllTrackxy_all2_9.root", "Read");
  TFile* f = new TFile("Output/"+RunNum+"_1stFit_all_Aligned.root", "RECREATE");
  TTree* myTree = new TTree("TrackInfo", "Selected tracks with minimum Chi2");

  TrackFit * tf;
  tf = new TrackFit();
     
  tf->Loop(InputFile, f, myTree);
  f->Close();
  delete tf; 
}
