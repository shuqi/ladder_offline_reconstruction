#ifndef _TrackFit_H
#define _TrackFit_H 1

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TRandom.h>
#include <TF1.h>
#include <TRandom3.h>
#include <TString.h>

// Header file for the classes stored in the TTree if any.
#include <vector>
using namespace std; 

class TrackFit {
public :
  TrackFit();
  virtual ~TrackFit();
  void     Loop(TFile* InputFile, TFile* f, TTree* myTree);

};

TrackFit::TrackFit()
{
  std::cout << "Begin" << std::endl;
}

TrackFit::~TrackFit(){}

#endif