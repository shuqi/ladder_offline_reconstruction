#include "TrackFit.h"

int main(int argc, char **argv)
{
   TString RunNum = argv[1];
   //TFile* InputFile = new TFile("/home/dell/ladder_Offline_Reconstruction/ladder_DESY_data/"+RunNum+"/Eff2/"+RunNum+"_AllTrackxy_all_Eff6_5Selected.root", "Read");
   
  TFile* InputFile = new TFile("/home/dell/ladder_Offline_Reconstruction/ladder_DESY_data/"+RunNum+"/Eff2/"+RunNum+"_AllTrackxy_all_Eff5.root", "Read");
  TFile* InputComFile = new TFile("/home/dell/ladder_Offline_Reconstruction/ladder_DESY_data/"+RunNum+"/Eff2/Clusterxy_all_Eff6.root", "Read");
  // TFile* f = new TFile("Output/"+RunNum+"_Aligned5planesEff_5found_Cut1.root", "RECREATE");
  TFile* f = new TFile("Output/"+RunNum+"_AlignedEff5_10pixels_PlaneID7.root", "RECREATE");
  TTree* myTree = new TTree("TrackInfo", "Selected tracks with minimum Chi2");

  TrackFit * tf;
  tf = new TrackFit();
     
  tf->Loop(InputFile, f, myTree, InputComFile);
  f->Close();
  delete tf; 
}
