#define TrackFit_cxx
#include "TrackFit.h"

#include <iostream>

#include <TStyle.h>
#include <TCanvas.h>
#include <TMinuit.h>
#include <TMatrix.h>
#include <TMatrixD.h>
#include <TSystem.h>
#include <TNtuple.h>
#include <TH1.h>
#include <TH2.h>
#include <TLorentzVector.h>
#include <TFitResult.h>
#include <TFitResultPtr.h>

const Int_t npts = 5; // chip nums
// 1 for chi2 fit and 2 for likelyhood fit

double xx[npts], yy[npts], zz[npts]; 
int plane[npts];
double sigmax=0.025/pow(12,0.5), sigmay=0.025/pow(12,0.5);

int option = 5;

void fcn_chisq(Int_t &npar, Double_t *gin, Double_t &f, Double_t *par, Int_t iflag)
{
  const Double_t a1 = par[0];
  const Double_t b1 = par[1];
  const Double_t a2 = par[2];
  const Double_t b2 = par[3];
  
  Double_t chisq=0.; 
  for( int i = 0; i < npts ; i++ ) {
    Double_t chi1  = ( a1*zz[i] + b1 - xx[i] )/sigmax; chi1 = chi1*chi1;   
    Double_t chi2  = ( a2*zz[i] + b2 - yy[i] )/sigmay; chi2 = chi2*chi2;   
    chisq += chi1 +chi2; 
  }	
  f = chisq;
}
 
void Fit(Double_t results[])
{
  TMinuit *gMinuit = new TMinuit(4);  //initialize TMinuit with a maximum of 3 params
  gMinuit->SetFCN(fcn_chisq );
	
  Int_t    ierflg = 0;
  Double_t arglist[10], val[10], err[10];
	
  arglist[0] = -1;
  gMinuit->mnexcm("SET PRI", arglist, 1, ierflg);
  arglist[0] =0.5;
  gMinuit->mnexcm("SET ERR", arglist, 1, ierflg);
	
  // Set starting values and step sizes for parameters
  static Double_t vstart[4] = { 0.0010, 1.,  0.0010, 1.};
  static Double_t   step[4] = { 0.0001, 0.1,  0.0001, 0.1};

  gMinuit->mnparm(0, "a1", vstart[0], step[0], 0, 0, ierflg);
  gMinuit->mnparm(1, "b1", vstart[1], step[1], 0, 0, ierflg);
  gMinuit->mnparm(2, "a2", vstart[2], step[2], 0, 0, ierflg);
  gMinuit->mnparm(3, "b2", vstart[3], step[3], 0, 0, ierflg);

  // Now ready for minimization step
  arglist[0] = 5000;
  arglist[1] = 1.00;
  gMinuit->mnexcm("MIGRAD", arglist , 1, ierflg); // mimimization here ... 
  //gMinuit->mnexcm("MINOS" , arglist , 2, ierflg); // Minos error 
	
  // Print results
  Double_t amin,edm,errdef;
  Int_t nvpar,nparx,icstat;
  gMinuit->mnstat(amin,edm,errdef,nvpar,nparx,icstat);
  //std::cout<<"nparx="<<nparx<<std::endl;

  TString chnam;
  Double_t xlolim, xuplim;
  Int_t iuext, iuint;
	
  for(Int_t p = 0; p < 4; p++)
  { 
    results[p] =0.0; 
    gMinuit->mnpout(p, chnam, val[p], err[p], xlolim, xuplim, iuint);
    // printf("%2d %5s %8.2f +/- %8.2f\n", p, chnam.Data(), val[p]*1000, err[p]*1000); 
    // printf("%2d %5s %8.2f +/- %8.2f\n", p, chnam.Data(), val[p], err[p]);
    results[p] = val[p]; 
  }
  delete gMinuit; 
}


void TrackFit::Loop(TFile* InputFile, TFile* f, TTree* myTree, TFile* InputComFile)
{ 
  std::cout << "checkaa" << std::endl;
  TTree *tInFile = (TTree*)InputComFile->Get("Event");
  vector<int>* vec_plane           = new std::vector<int>();
  tInFile->SetBranchAddress("planeID", &vec_plane);
  vector<double>* vec_xglo         = new std::vector<double>();
  tInFile->SetBranchAddress("xglo", &vec_xglo);
  vector<double>* vec_yglo         = new std::vector<double>();
  tInFile->SetBranchAddress("yglo", &vec_yglo);
  vector<double>* vec_zglo         = new std::vector<double>();
  tInFile->SetBranchAddress("zglo", &vec_zglo);
  vector<int>* vec_hitsNum         = new std::vector<int>();
  tInFile->SetBranchAddress("hitsNum", &vec_hitsNum);
  vector<int>* vec_hitsNumX      = new std::vector<int>(); 
  tInFile->SetBranchAddress("hitsNumX", &vec_hitsNumX);
  vector<int>* vec_hitsNumY      = new std::vector<int>();
  tInFile->SetBranchAddress("hitsNumY", &vec_hitsNumY);

  vector<double>* vec_col = new std::vector<double>();
  vector<double>* vec_row = new std::vector<double>();
  tInFile->SetBranchAddress("col", &vec_col);
  tInFile->SetBranchAddress("row", &vec_row);

  TTree* fChain = (TTree*)InputFile->Get("TrackInfo");
  int TrackSize;
  vector<int>* planeID = new std::vector<int>();
  vector<double>* col = new std::vector<double>();
  vector<double>* row = new std::vector<double>();
  vector<double>* xloc = new std::vector<double>();
  vector<double>* yloc = new std::vector<double>();
  vector<double>* xglo = new std::vector<double>();
  vector<double>* yglo = new std::vector<double>();
  vector<double>* zglo = new std::vector<double>();

  fChain->SetBranchAddress("TrackSize", &TrackSize);
  fChain->SetBranchAddress("planeID", &planeID);
  fChain->SetBranchAddress("col", &col);
  fChain->SetBranchAddress("row", &row);
  fChain->SetBranchAddress("xloc", &xloc);
  fChain->SetBranchAddress("yloc", &yloc);
  fChain->SetBranchAddress("xglo", &xglo);
  fChain->SetBranchAddress("yglo", &yglo);
  fChain->SetBranchAddress("zglo", &zglo);
   
  f->cd();
  double Chi2_Out, Chi2_OutY, Chi2_OutX;
  myTree->Branch("Chi2", &Chi2_Out);
  myTree->Branch("Chi2X", &Chi2_OutX);
  myTree->Branch("Chi2Y", &Chi2_OutY);

  vector<int>* vec_planeOut = new std::vector<int>();
  myTree->Branch("planeID", &vec_planeOut);

  vector<double>* vec_xlocOut = new std::vector<double>();
  myTree->Branch("xloc", &vec_xlocOut);

  vector<double>* vec_ylocOut = new std::vector<double>();
  myTree->Branch("yloc", &vec_ylocOut);
  TH1D * res[npts];
  TH1D * residual[npts][2];

  TString namea1 = "a1";
  TString nameb1 = "b1";
  TString namea2 = "a2";
  TString nameb2 = "b2";

  res[0] =  new TH1D(namea1, "", 100, -.001, .001);     
  res[2] =  new TH1D(namea2, "", 100, -.001, .001);
  res[1] =  new TH1D(nameb1, "", 100, -10, 10);
  res[3] =  new TH1D(nameb2, "", 100, -10, 10);
  double lowx, highx, lowy, highy;
  for (int m=0; m < npts; m++){
    TString nameX = "residual_plane"+to_string(m)+"_x";
    TString nameY = "residual_plane"+to_string(m)+"_y";

    residual[m][0] = new TH1D(nameX, "", 50, -0.03, 0.03);

    residual[m][1] = new TH1D(nameY, "", 50, -0.03, 0.03);
  }
  TH2D* hitmap;
  TH2D* hitmap2; 
/*
  hitmap = new TH2D("found6", "", 1024, 0, 1024, 512, 0, 512);
  hitmap2 = new TH2D("found6_in", "", 1024, 0, 1024, 512, 0, 512);
*/
  hitmap = new TH2D("found6", "", 100, 0, 1000, 50, 0, 500);
  hitmap2 = new TH2D("found6_in", "", 100, 0, 1000, 50, 0, 500);

  int nentries = (int)fChain->GetEntries();
  double residualX;
  double residualY;
  Double_t results[4]; 

  std::vector<double> vec_chi2;
  std::vector<double> vec_chi2X;
  std::vector<double> vec_chi2Y;
  std::vector<double> vec_a1;
  std::vector<double> vec_a2;
  std::vector<double> vec_b1;
  std::vector<double> vec_b2;
  std::vector<double> xgloInSelPlane;
  std::vector<double> ygloInSelPlane;
  std::vector<double> zgloInSelPlane;
  std::vector<int> hitsNumInSelPlane;

  std::vector<double> dxInSelPlane;
  std::vector<double> dyInSelPlane;
  std::vector<double> dxyInSelPlane;

  std::vector<double> colInSelPlane;
  std::vector<double> rowInSelPlane;

  int TotalTracks = 0;
  int SelTracks_100 = 0;
  int SelTracks = 0;
  for (Long64_t jentry = 0; jentry < nentries; jentry++) {
    
    if(jentry%10000 == 0) std::cout<<"Processing event " <<jentry<<"..."<<std::endl;
    // if(jentry > 100) break;
    fChain->GetEntry(jentry); 
    

    vec_planeOut->clear();
    vec_xlocOut->clear();
    vec_ylocOut->clear(); 

    tInFile->GetEntry(jentry); 
    /////
    xgloInSelPlane.clear();
    ygloInSelPlane.clear();
    zgloInSelPlane.clear();
    hitsNumInSelPlane.clear();

    dxInSelPlane.clear();
    dyInSelPlane.clear();
    dxyInSelPlane.clear();

    rowInSelPlane.clear();
    colInSelPlane.clear();

    std::vector<int> Selplane = findVectors(vec_plane, 0);
    if (option == 6){
    for (int j = Selplane[0]; j < Selplane[Selplane.size() - 1] + 1; j++)
    {
      xgloInSelPlane.push_back(vec_xglo->at(j));
      ygloInSelPlane.push_back(vec_yglo->at(j));
      zgloInSelPlane.push_back(vec_zglo->at(j));
      hitsNumInSelPlane.push_back(vec_hitsNum->at(j));
      rowInSelPlane.push_back(vec_row->at(j));
      colInSelPlane.push_back(vec_col->at(j));
    }
    }
    /////
    vec_chi2.clear();
    vec_chi2X.clear();
    vec_chi2Y.clear();
    vec_a1.clear();
    vec_b1.clear();
    vec_a2.clear();
    vec_b2.clear();

    for (int i = 0; i < TrackSize*npts; i+=npts)
    {
      double chi2 = 0;
      double chi2x = 0;
      double chi2y = 0;

      for (int j = 0; j < npts; j++)
      {
        plane[j] = planeID->at(i+j);
	      xx[j] = xglo->at(i+j);
	      yy[j] = yglo->at(i+j);
        zz[j] = zglo->at(i+j);  
      } 

      Fit(results);
      for (int k = 0; k < 5; k++)
      {
        double dx = results[0]*zz[k] + results[1]-xx[k];
        double dy = results[2]*zz[k] + results[3]-yy[k];
	      chi2 += (dx * dx) / (sigmax*sigmax) + (dy * dy) / (sigmay*sigmay);
        chi2x += (dx * dx) / (sigmax*sigmax);
        chi2y += (dy * dy) / (sigmay*sigmay);
      }

      vec_chi2.push_back(chi2);
      vec_chi2X.push_back(chi2x);
      vec_chi2Y.push_back(chi2y);
      vec_a1.push_back(results[0]);
      vec_b1.push_back(results[1]);
      vec_a2.push_back(results[2]);
      vec_b2.push_back(results[3]);
    }
    
    
    // for (int i = 0; i < TrackSize; i++) std::cout << "vec_chi2 " << i << " = " << vec_chi2[i]<< std::endl;
    double minChi2 = *min_element(vec_chi2.begin(), vec_chi2.end());
    int minPos = min_element(vec_chi2.begin(), vec_chi2.end()) - vec_chi2.begin();

    double minChi2x = *min_element(vec_chi2X.begin(), vec_chi2X.end());
    int minPosx = min_element(vec_chi2X.begin(), vec_chi2X.end()) - vec_chi2X.begin();

    double minChi2y = *min_element(vec_chi2Y.begin(), vec_chi2Y.end());
    int minPosy = min_element(vec_chi2Y.begin(), vec_chi2Y.end()) - vec_chi2Y.begin();
     
    if (minPos != minPosx || minPos != minPosy) continue;
    if (minChi2/6 > 100) continue; 
    TotalTracks++;
    Chi2_Out = minChi2;
    Chi2_OutX = minChi2x;
    Chi2_OutY = minChi2y;
    //////////////
    
    if (option == 5)
    {
      double xpre = vec_a1[minPos]*37.34036593 + vec_b1[minPos];
      double ypre = vec_a2[minPos]*37.34036593 + vec_b2[minPos];
      //double xpre = vec_a1[minPos]*20 + vec_b1[minPos];
      //double ypre = vec_a2[minPos]*20 + vec_b1[minPos];
      double colpre = (xpre*1000)/25+512;
      double rowpre = (ypre*1000)/25+256;
      if(rowpre < 38) 
      {//continue; 
      }
      hitmap->Fill(colpre, rowpre);
    }
    if (option == 6){
    for (int kk = 0; kk < Selplane.size(); kk++)
    {
      double ddx = vec_a1[minPos]*zgloInSelPlane[kk] + vec_b1[minPos] - xgloInSelPlane[kk];
      double ddy = vec_a2[minPos]*zgloInSelPlane[kk] + vec_b2[minPos] - ygloInSelPlane[kk];
      // std::cout << "ddx = " << ddx << std::endl;
      // std::cout << "ddy = " << ddy << std::endl;
      dxInSelPlane.push_back(std::fabs(ddx));
      dyInSelPlane.push_back(std::fabs(ddy));
      dxyInSelPlane.push_back(std::sqrt(ddy*ddy + ddx*ddx));
    }

    double mindx = *min_element(dxInSelPlane.begin(), dxInSelPlane.end());
    double mindy = *min_element(dyInSelPlane.begin(), dyInSelPlane.end());
    double mindxy = *min_element(dxyInSelPlane.begin(), dxyInSelPlane.end());
    int mindxPos = min_element(dxInSelPlane.begin(), dxInSelPlane.end()) - dxInSelPlane.begin();
    int mindyPos = min_element(dyInSelPlane.begin(), dyInSelPlane.end()) - dyInSelPlane.begin();
    int mindxyPos = min_element(dxyInSelPlane.begin(), dxyInSelPlane.end()) - dxyInSelPlane.begin();
    // if (mindxPos != mindyPos) std::cout << "mindxPos != mindyPos" << std::endl;
    //std::cout << "std::fabs(mindx) = " << std::fabs(mindx) << std::endl;
    if (std::fabs(mindx) < 0.1 && std::fabs(mindy) < 0.1)
    {
      SelTracks_100++;
      //std::cout << "std::fabs(mindx) = " << std::fabs(mindx) << std::endl;
      hitmap->Fill(colInSelPlane[mindxyPos], rowInSelPlane[mindxyPos]);
    }
    SelTracks++;
    hitmap2->Fill(colInSelPlane[mindxyPos], rowInSelPlane[mindxyPos]);
    //found6in
    }
    /////////////
   for (int l = 0; l < npts; l++)
    {
      vec_planeOut->push_back(planeID->at(minPos*npts + l));
      vec_xlocOut->push_back(xloc->at(minPos*npts + l));
      vec_ylocOut->push_back(yloc->at(minPos*npts + l));
       
      residualX = vec_a1[minPos]*zglo->at(minPos*npts + l) + vec_b1[minPos] - xglo->at(minPos*npts + l);
      residualY = vec_a2[minPos]*zglo->at(minPos*npts + l) + vec_b2[minPos] - yglo->at(minPos*npts + l);

      residual[l][0] -> Fill(residualX);
      residual[l][1] -> Fill(residualY);
    }
    myTree->Fill();
  }
  myTree->Write();
  hitmap->Write();
  hitmap2->Write();
  std::cout << "TotalTracks = " << TotalTracks << std::endl;
  std::cout << "SelTracks = " << SelTracks << std::endl;
  std::cout << "SelTracks_100 = " << SelTracks_100 << std::endl;
  std::cout << "ratio = " << (double)SelTracks/TotalTracks << std::endl;
  for (int j = 0; j < npts; j++)
  {
    residual[j][0] -> Fit("gaus");
    residual[j][1] -> Fit("gaus");
    residual[j][0] -> Write();
    residual[j][1] -> Write();  
  }
}
