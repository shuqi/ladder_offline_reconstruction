import sys 
import os
import ROOT
from math import sqrt, exp
from array import array

# execfile("/home/dell/RootUtils/AtlasStyle.py")
# execfile("/home/dell/RootUtils/AtlasUtils.py")

def main(RunNum, dd):
    file1 = ROOT.TFile("./Output/Run"+RunNum+"_1stFit_mhit.root")
    if dd == "x":
        direction = "xdirec"
    else:
        direction = "ydirec"

    c = ROOT.TCanvas("c", "", 800, 1200)
    c.Divide(2,3)
    for i in range (0, 6):
        c.cd()
        h2 = file1.Get('residual_plane'+str(i)+'_'+dd)
        h2.Scale(1/h2.Integral())
        # c = ROOT.TCanvas("c"+str(i), "", 850, 650)
        if i == 0:
            a1 = 0
            a2 = 0.7 
            a3 = 0.5
            a4 = 1
        elif i == 1:
            a1 = 0.5
            a2 = 0.7
            a3 = 1
            a4 = 1
        elif i == 2:
            a1 = 0
            a2 = 0.35
            a3 = 0.5
            a4 = 0.65
        elif i == 3:
            a1 = 0.5
            a2 = 0.35
            a3 = 1
            a4 = 0.65
        elif i == 4:
            a1 = 0
            a2 = 0
            a3 = 0.5
            a4 = 0.3
        elif i == 5:
            a1 = 0.5
            a2 = 0
            a3 = 1
            a4 = 0.3
        pad = ROOT.TPad("pad"+str(i), "", a1, a2, a3, a4)
        pad.SetTopMargin(0.15);
        pad.SetBottomMargin(0.15)
        pad.SetLeftMargin(0.15)
        pad.SetRightMargin(0.15)
        pad.Draw()
        pad.cd()
        ROOT.gStyle.SetOptStat(0)

        #h2.SetLineStyle(ROOT.kDashed)
        h2.SetLineColor(ROOT.kRed-6)
        h2.SetMarkerSize(0)

        trackNum = h2.GetEntries()
 
        res_aft = h2.Fit("gaus", "QS");
        err_aft = res_aft.Parameter(2)*1e3
        err_aft_err = res_aft.Error(2)*1e3

        print("Run0"+RunNum + " sigma" + str(round(i, 2)) + " "+dd+" = " + str(round(err_aft,2)))
    
 
        f2 = h2.GetFunction('gaus')
        f2.SetLineColor(ROOT.kRed-4)

   
        h2.SetMaximum(h2.GetMaximum()*1.3)
        h2.Draw("hist")
   
        tx0 = ROOT.TLatex()
        tx0.SetTextSize(0.035)
        tx0.SetTextFont(22)
        tx0.DrawLatexNDC(0.2, 0.75, "Run0"+RunNum)
        tx1 = ROOT.TLatex()
        tx1.SetTextSize(0.035)
        tx1.SetTextFont(22)
        #tx1.DrawLatexNDC(0.2, 0.69, "Chi2 < "+ chi2)
        tx2 = ROOT.TLatex()
        tx2.SetTextSize(0.035)
        tx2.SetTextFont(22)
        #tx2.DrawLatexNDC(0.2, 0.63, str(trackNum)+" Tracks")
        tx3 = ROOT.TLatex()
        tx3.SetTextSize(0.035)
        tx3.SetTextFont(2)
        tx3.SetTextColor(ROOT.kRed-4)
        # tx3.DrawLatexNDC(0.55, 0.75, "#sigma_{after}   = " + str(round(err_aft, 2)) + " +/- " + str(round(err_aft_err, 2)) + " um")

        legend = ROOT.TLegend(0.6, 0.75, 0.88, 0.88);
        legend.AddEntry(h2,"data","pl");
        legend.AddEntry(f2, "fit", "pl")
        ROOT.gStyle.SetLegendBorderSize(0)
        # legend.Draw("");

        tx3.DrawLatexNDC(0.55, 0.75, "#sigma_{before}   = " + str(round(err_aft, 2)) + " +/- " + str(round(err_aft_err, 2)) + " um")
    
        h2.SetTitle(dd +"direction, ChipID = " + str(i))
        h2.GetYaxis().SetTitle("Events")
        h2.GetXaxis().SetTitle("residual("+dd+"_{meas} - "+dd+"_{predict}) [mm]")

        # c.SaveAs('./plots/Run0'+RunNum+'/'+ direction + '/' + dd + str(i) +'_Cut' + chi2 +'_1hit_6Chi2.pdf')
        del h2
        del pad
    c.SaveAs('./plots/Run0'+RunNum+'/'+ direction + '_mhit_scale1.pdf')
    file1.Close()

        
RunNum = "343"
#main(RunNum, "20", "x")
#main(RunNum, "10", "x")
main(RunNum, "x")

#main(RunNum, "20", "y")
#main(RunNum, "10", "y")
main(RunNum, "y")
