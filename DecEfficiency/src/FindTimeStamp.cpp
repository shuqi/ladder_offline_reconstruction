#include "IncludeFun.h"
#include "constants.h"
#include "FindTimeStamp.h"

#include <iostream>
#include <fstream>
#include <vector>

// Get the same FPGAtimeStamp
FindTimeStamp::FindTimeStamp()
{
  std::cout << "**********************************" << std::endl;
  std::cout << "*** finding FPGA time matched ***"  << std::endl;
  std::cout << "**********************************" << std::endl;
}

FindTimeStamp::~FindTimeStamp()
{

}

bool FindTimeStamp::initialize(TString directory, TString RunNumber)
{
  Infile = new TFile(directory+RunNumber+"/Outputhit2_9.root", "read"); 
  // Infile = new TFile("/media/dell/My Passport/TB_04_2023/"+RunNumber+"/Outputhit2_9.root", "read");

  return true;
}

std::vector<std::vector<int>> FindTimeStamp::FindTurnPoint()
{
  std::vector<std::vector<int>> vec_Findpos;
  // std::cout << "1" << std::endl;
  std::vector<int> vec_Findpos1 = FindZero(Infile, 5);
  // std::cout << "2" << std::endl;
  std::vector<int> vec_Findpos2 = FindZero(Infile, 6);
  // std::cout << "3" << std::endl;
  std::vector<int> vec_Findpos3 = FindZero(Infile, 7);
  // std::cout << "4" << std::endl;
  std::vector<int> vec_Findpos4 = FindZero(Infile, 8);
  // std::cout << "5" << std::endl;
  std::vector<int> vec_Findpos5 = FindZero(Infile, 9);
  // std::cout << "6" << std::endl;
  std::vector<int> vec_Findpos6 = FindZero(Infile, 10);

  std::cout << vec_Findpos1.size() << std::endl;
  std::cout << vec_Findpos2.size() << std::endl;
  std::cout << vec_Findpos3.size() << std::endl;
  std::cout << vec_Findpos4.size() << std::endl;
  std::cout << vec_Findpos5.size() << std::endl;
  std::cout << vec_Findpos6.size() << std::endl;

  std::vector<int> loopNum;
  loopNum.push_back(vec_Findpos1.size());
  loopNum.push_back(vec_Findpos2.size());
  loopNum.push_back(vec_Findpos3.size());
  loopNum.push_back(vec_Findpos4.size());
  loopNum.push_back(vec_Findpos5.size());
  loopNum.push_back(vec_Findpos6.size());

  maxLoop = *max_element(loopNum.begin(), loopNum.end()); 
  minLoop = *min_element(loopNum.begin(), loopNum.end()); 

  maxPosLoop = max_element(loopNum.begin(),loopNum.end()) - loopNum.begin();
  minPosLoop = min_element(loopNum.begin(),loopNum.end()) - loopNum.begin();

  std::cout << "minLoop = " << minLoop << std::endl;
  std::cout << "maxLoop = " << maxLoop << std::endl;

  std::cout << "minPosLoop = " << minPosLoop << std::endl;
  std::cout << "maxPosLoop = " << maxPosLoop << std::endl;

  // for (int j = 0; j < minLoop; j++)
  // {
  //   std::cout << vec_Findpos1[j] << std::endl;
  //   std::cout << vec_Findpos2[j] << std::endl;
  //   std::cout << vec_Findpos3[j] << std::endl;
  //   std::cout << vec_Findpos4[j] << std::endl;
  //   std::cout << vec_Findpos5[j] << std::endl;
  //   std::cout << vec_Findpos6[j] << std::endl;
  //   std::cout << "=======" << std::endl;
  // }

  vec_Findpos.push_back(vec_Findpos1);
  vec_Findpos.push_back(vec_Findpos2);
  vec_Findpos.push_back(vec_Findpos3);
  vec_Findpos.push_back(vec_Findpos4);
  vec_Findpos.push_back(vec_Findpos5);
  vec_Findpos.push_back(vec_Findpos6);

  return vec_Findpos;
}


void FindTimeStamp::FindTime(std::vector<std::vector<int>> vec_Findpos, TString directory, TString RunNumber){

    TFile *OutFile = new TFile(directory+RunNumber+"/Eff2/SameTimeLooseFirst4BoardEff.root", "RECREATE");
    TTree* myTree = new TTree("TimeInfo", "beam data");

    TTree *t1 = (TTree*)Infile->Get("HitsInfo_10");   // Chip05   chip06
    TTree *t2 = (TTree*)Infile->Get("HitsInfo_9");   // Chip04
    TTree *t3 = (TTree*)Infile->Get("HitsInfo_8");   // Chip03
    TTree *t4 = (TTree*)Infile->Get("HitsInfo_6");   // Chip01   in order to calculate efficiency skipping chip02


    std::vector<int>* vec_pos = new std::vector<int>();
    myTree->Branch("pointor", &vec_pos);
    std::vector<int>* vec_timeFPGA = new std::vector<int>();
    myTree->Branch("timeFPGA", &vec_timeFPGA);
    std::vector<int>* vec_timeChip = new std::vector<int>();
    myTree->Branch("timeChip", &vec_timeChip);
    std::vector<int>* vec_row      = new std::vector<int>();
    myTree->Branch("row", &vec_row);
    std::vector<int>* vec_col      = new std::vector<int>();
    myTree->Branch("col", &vec_col);
    std::vector<int>* vec_plane      = new std::vector<int>();
    myTree->Branch("planeID", &vec_plane);

    int row1, row2, row3, row4, col1, col2, col3, col4, timeFPGA1, timeFPGA2, timeFPGA3, timeFPGA4, timeChip1, timeChip2, timeChip3, timeChip4;
    int plandID1, plandID2, plandID3, plandID4;

    t1->SetBranchAddress("chipID", &plandID1);
    t1->SetBranchAddress("row", &row1);
    t1->SetBranchAddress("col", &col1);
    t1->SetBranchAddress("timeFPGA", &timeFPGA1);
    t1->SetBranchAddress("timeChip", &timeChip1);
    
    t2->SetBranchAddress("chipID", &plandID2);
    t2->SetBranchAddress("row", &row2);
    t2->SetBranchAddress("col", &col2);
    t2->SetBranchAddress("timeFPGA", &timeFPGA2);
    t2->SetBranchAddress("timeChip", &timeChip2);

    t3->SetBranchAddress("chipID", &plandID3);
    t3->SetBranchAddress("row", &row3);
    t3->SetBranchAddress("col", &col3);
    t3->SetBranchAddress("timeFPGA", &timeFPGA3);
    t3->SetBranchAddress("timeChip", &timeChip3);

    t4->SetBranchAddress("chipID", &plandID4);
    t4->SetBranchAddress("row", &row4);
    t4->SetBranchAddress("col", &col4);
    t4->SetBranchAddress("timeFPGA", &timeFPGA4);
    t4->SetBranchAddress("timeChip", &timeChip4);

    int TotHitsNum4 = (int)t4->GetEntries();
    int TotHitsNum3 = (int)t3->GetEntries();
    int TotHitsNum2 = (int)t2->GetEntries();
    int TotHitsNum1 = (int)t1->GetEntries();

    std::cout << "TotHitsNum of chip1 = " << TotHitsNum4 << std::endl;
    std::cout << "TotHitsNum of chip2 = " << TotHitsNum3 << std::endl;
    std::cout << "TotHitsNum of chip3 = " << TotHitsNum2 << std::endl;
    std::cout << "TotHitsNum of chip4 = " << TotHitsNum1 << std::endl;

    int ltag = 0;
    int ktag = 0;
    int jtag = 0;
    
    int preFPGA = 0;
    for (int i = 0; i < (int)t1->GetEntries(); i++)
    {
      t1->GetEntry(i);
      if (i%100000 == 0) std::cout << "i = " << i << std::endl;
      // if (timeFPGA1 <= 85000000) continue;
      if (timeFPGA1 < 29500000) continue;
      // if (timeFPGA1 > 30000000) break;   //Run0674  6x106 - 14*106
      // if (timeFPGA1 > 19500000) break;
      // if (i > 100000) break;
      int Items = findItems(&vec_Findpos[maxPosLoop], i);
      if (Items >= 0 && (minLoop == maxLoop))
      {
        // break;
        std::cout << "found...." << std::endl;
        jtag = vec_Findpos[2][Items];   // 3th chip
        ktag = vec_Findpos[1][Items];   // 2th chip
        ltag = vec_Findpos[0][Items];   // 1th chip

        std::cout << "jtag = " << jtag << std::endl;
        std::cout << "ktag = " << ktag << std::endl;
        std::cout << "ltag = " << ltag << std::endl;
      }
      else if (Items >= 0 && (minLoop != maxLoop))
      {
        std::cout << "found...." << std::endl;
        std::cout << "but minLoop != maxLoop" << std::endl;
        if (Items == minLoop)
        {
          break;
        }
        else
        {
          jtag = vec_Findpos[2][Items];   // 3th chip
          ktag = vec_Findpos[1][Items];   // 2th chip
          ltag = vec_Findpos[0][Items];   // 1th chip  
        }
      }
      for (int j = jtag; j < (int)t2->GetEntries(); j++)
      {
        t2->GetEntry(j);
        if (passTime(timeFPGA1, timeFPGA2, 1))
        {
          jtag = j + 1;
          for (int k = ktag; k < (int)t3->GetEntries(); k++)
          {
            t3->GetEntry(k);
            if (passTime(timeFPGA2, timeFPGA3, 1))
            {
              ktag = k + 1;
              vec_timeFPGA->clear();
              vec_timeChip->clear();
              vec_row->clear();
              vec_col->clear();
              vec_plane->clear();
              vec_pos->clear();

              int SavetimeFPGA1Times = 0;
              for (int l = ltag; l < (int)t4->GetEntries(); l++)
              {
                t4->GetEntry(l);
                if (passTime(timeFPGA3, timeFPGA4, 1))
                {
                  ltag = l + 1;
                  // std::cout << "passTime(timeFPGA3, timeFPGA4)" << std::endl;
                  if ( SavetimeFPGA1Times == 0)
                  {
                    vec_timeFPGA->push_back(timeFPGA1);
                    vec_timeChip->push_back(timeChip1);
                    vec_row->push_back(row1);
                    vec_col->push_back(col1);
                    vec_plane->push_back(5);
                    vec_pos->push_back(i);

                    vec_timeFPGA->push_back(timeFPGA2);
                    vec_timeChip->push_back(timeChip2);
                    vec_row->push_back(row2);
                    vec_col->push_back(col2);
                    vec_plane->push_back(4);
                    vec_pos->push_back(j);

                    vec_timeFPGA->push_back(timeFPGA3);
                    vec_timeChip->push_back(timeChip3);
                    vec_row->push_back(row3);
                    vec_col->push_back(col3);
                    vec_plane->push_back(3);
                    vec_pos->push_back(k);
                    SavetimeFPGA1Times++;
                  }

                  vec_timeFPGA->push_back(timeFPGA4);
                  vec_timeChip->push_back(timeChip4);
                  vec_row->push_back(row4);
                  vec_col->push_back(col4);
                  vec_plane->push_back(1);
                  vec_pos->push_back(l);
                }
                else if (passbreak(timeFPGA3, timeFPGA4, 1)) { ltag = l; break;}
                else if (passcontinue(timeFPGA3, timeFPGA4, 1)) continue;
              }
              if(vec_timeFPGA->size() > 0)  myTree->Fill();
            }
            else if (passbreak(timeFPGA2, timeFPGA3, 1)) { ktag = k; break;}
            else if (passcontinue(timeFPGA2, timeFPGA3, 1)) { continue; }
          }
        }
        else if (passbreak(timeFPGA1, timeFPGA2, 1)) { jtag = j; break;}
        else if (passcontinue(timeFPGA1, timeFPGA2, 1)) continue;
      }
    }
  myTree->Write();
  OutFile->Close();
  
}
