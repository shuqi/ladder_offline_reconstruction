#include <iostream>
#include <vector>
#include <algorithm>

#include "IncludeFun.h"
#include "CombineAllTracks.h"

using namespace std;

CombineAllTracks::CombineAllTracks()
{
  std::cout << "************************************" << std::endl;
  std::cout << "*** combining all possible tracks***"  << std::endl;
  std::cout << "************************************" << std::endl;
}

CombineAllTracks::~CombineAllTracks()
{

}

void CombineAllTracks::CombineCluster(TString directory, TString RunNumber, TString hitOption, TString EffOption){

    // TFile *InFile = new TFile(directory+RunNumber+"/Aligned/Clusterxy_"+hitOption+"_Eff6_5Selected.root", "read");
    TFile *InFile = new TFile(directory+RunNumber+"/Eff2/Clusterxy_"+hitOption+"_"+EffOption+".root", "read");
    TTree *tInFile = (TTree*)InFile->Get("Event");
    int EventNum;
    tInFile->SetBranchAddress("EventNum", &EventNum);
    int TotalClusters;
    tInFile->SetBranchAddress("TotClusterNum", &TotalClusters);
    vector<int>* vec_plane           = new std::vector<int>();
    tInFile->SetBranchAddress("planeID", &vec_plane);
    vector<int>* vec_ClusterNum      = new std::vector<int>();
    tInFile->SetBranchAddress("clusterNum", &vec_ClusterNum);
    vector<double>* vec_col          = new std::vector<double>();
    tInFile->SetBranchAddress("col", &vec_col);
    vector<double>* vec_row          = new std::vector<double>();
    tInFile->SetBranchAddress("row", &vec_row);
    vector<int>* vec_hitsNum         = new std::vector<int>();
    tInFile->SetBranchAddress("hitsNum", &vec_hitsNum);
    vector<int>* vec_hitsNumX      = new std::vector<int>(); 
    tInFile->SetBranchAddress("hitsNumX", &vec_hitsNumX);
    vector<int>* vec_hitsNumY      = new std::vector<int>();
    tInFile->SetBranchAddress("hitsNumY", &vec_hitsNumY);
    vector<double>* vec_xloc         = new std::vector<double>();
    tInFile->SetBranchAddress("xloc", &vec_xloc);
    vector<double>* vec_yloc         = new std::vector<double>();
    tInFile->SetBranchAddress("yloc", &vec_yloc);
    vector<double>* vec_xglo         = new std::vector<double>();
    tInFile->SetBranchAddress("xglo", &vec_xglo);
    vector<double>* vec_yglo         = new std::vector<double>();
    tInFile->SetBranchAddress("yglo", &vec_yglo);
    vector<double>* vec_zglo         = new std::vector<double>();
    tInFile->SetBranchAddress("zglo", &vec_zglo);
    // TFile *OutFile = new TFile(directory+RunNumber+"/Aligned/"+RunNumber+"_AllTrackxy_"+hitOption+"_Eff6_5Selected.root", "RECREATE");
    TFile *OutFile = new TFile(directory+RunNumber+"/Eff2/"+RunNumber+"_AllTrackxy_"+hitOption+"_"+EffOption+".root", "RECREATE");
    // Out Tree
    TTree* myTree = new TTree("TrackInfo", "Roughly selected Tracks");

    int EventNumOut;
    myTree->Branch("EventNum", &EventNumOut);

    int TrackSize;
    myTree->Branch("TrackSize", &TrackSize);

    vector<int>* vec_planeOut        = new std::vector<int>();
    myTree->Branch("planeID", &vec_planeOut);
    vector<double>* vec_colOut       = new std::vector<double>();
    myTree->Branch("col", &vec_colOut);
    vector<double>* vec_rowOut       = new std::vector<double>();
    myTree->Branch("row", &vec_rowOut);
    vector<double>* vec_xlocOut      = new std::vector<double>();
    myTree->Branch("xloc", &vec_xlocOut);
    vector<double>* vec_ylocOut      = new std::vector<double>();
    myTree->Branch("yloc", &vec_ylocOut);
    vector<double>* vec_xgloOut      = new std::vector<double>();
    myTree->Branch("xglo", &vec_xgloOut);
    vector<double>* vec_ygloOut      = new std::vector<double>();
    myTree->Branch("yglo", &vec_ygloOut);
    vector<double>* vec_zgloOut      = new std::vector<double>();
    myTree->Branch("zglo", &vec_zgloOut);
    vector<int>* vec_hitsNumOut         = new std::vector<int>();
    myTree->Branch("hitsNum", &vec_hitsNumOut);
    vector<int>* vec_hitsNumXOut      = new std::vector<int>(); 
    myTree->Branch("hitsNumX", &vec_hitsNumXOut);
    vector<int>* vec_hitsNumYOut      = new std::vector<int>();
    myTree->Branch("hitsNumY", &vec_hitsNumYOut);

    int first_dimension;
    int fitPlane = 5;
    for (int m = 0; m < (int)tInFile->GetEntries(); m++){
      tInFile->GetEntry(m);
      if (m%10000 == 0) std::cout << "Processing " << m << "..." << std::endl;
      EventNumOut = EventNum;
      // if (m > 530000) break;
      vec_rowOut->clear();
      vec_colOut->clear();
      vec_planeOut->clear();
      vec_xlocOut->clear();
      vec_ylocOut->clear();
      vec_xgloOut->clear();
      vec_ygloOut->clear();
      vec_zgloOut->clear();
      vec_hitsNumOut->clear();
      vec_hitsNumXOut->clear();
      vec_hitsNumYOut->clear();

      first_dimension = 1;
      vector<vector<int>> Selplane;
      
      for (int i = 0; i < 5; i++)
      {
        int m = i;
        if (i >= 2) m = i+1;
        Selplane.push_back(findVectors(vec_plane, m));
        first_dimension *= Selplane[i].size();
      }

      // for (int i = 0; i < fitPlane; i++)
      // {
      //   Selplane.push_back(findVectors(vec_plane, i));
      //   first_dimension *= Selplane[i].size();
      // }

      TrackSize = first_dimension;
      
      int result[first_dimension][5];
      get_all_combination(Selplane, result);
      
      for (int i = 0; i < first_dimension; i++)
      {
        for (int j = 0; j < fitPlane; j++)
        {
          // cout<<result[i][j]<<"\t";
          int Selposition = result[i][j];
          vec_planeOut->push_back(j);
          vec_rowOut->push_back(vec_row->at(Selposition));
          vec_colOut->push_back(vec_col->at(Selposition));
          vec_xlocOut->push_back(vec_xloc->at(Selposition));
          vec_ylocOut->push_back(vec_yloc->at(Selposition));
          vec_xgloOut->push_back(vec_xglo->at(Selposition));
          vec_ygloOut->push_back(vec_yglo->at(Selposition));
          vec_zgloOut->push_back(vec_zglo->at(Selposition));
          vec_hitsNumOut->push_back(vec_hitsNum->at(Selposition));
          vec_hitsNumXOut->push_back(vec_hitsNumX->at(Selposition));
          vec_hitsNumYOut->push_back(vec_hitsNumY->at(Selposition));
        }
        // cout<<endl;
      }
      // std::cout << "first_dimension = " << first_dimension << std::endl;
      myTree->Fill();
  }
  myTree->Write();
  InFile->Close();
  OutFile->Close();
}