#include "IncludeFun.h"
#include "constants.h"
#include "CreateCluster.h"


CreateCluster::CreateCluster()
{
  std::cout << "********************************************" << std::endl;
  std::cout << "*** Calculation cluster position from all hits ***"  << std::endl;
  std::cout << "********************************************" << std::endl;
}

CreateCluster::~CreateCluster()
{

}

void CreateCluster::FormCluster(TString directory, TString RunNumber, TString hitOption, TString EffOption){
  
    TString filename = "/home/dell/ladder_Offline_Align/build/OutputFiles/"+RunNumber+"/Alignment_out_"+RunNumber+"_dn.dat";

    TFile *InFile;
    if (EffOption == "Eff6_5Selected")
    {
      InFile = new TFile(directory+RunNumber+"/Eff2/AllSameTimeHitsLooseEff6.root", "read");
    }
    else{
      InFile = new TFile(directory+RunNumber+"/Eff2/AllSameTimeHitsLoose"+EffOption+".root", "read");
    }
    
    TTree *tInFile = (TTree*)InFile->Get("TimeInfo");

    int EventNumber;
    tInFile->SetBranchAddress("EventNum", &EventNumber);
    std::vector<int>* vec_timeFPGA = new std::vector<int>();
    tInFile->SetBranchAddress("timeFPGA", &vec_timeFPGA);
    std::vector<int>* vec_timeChip = new std::vector<int>();
    tInFile->SetBranchAddress("timeChip", &vec_timeChip);
    std::vector<int>* vec_row      = new std::vector<int>();
    tInFile->SetBranchAddress("row", &vec_row);
    std::vector<int>* vec_col      = new std::vector<int>();
    tInFile->SetBranchAddress("col", &vec_col);
    std::vector<int>* vec_plane      = new std::vector<int>();
    tInFile->SetBranchAddress("planeID", &vec_plane);
    std::vector<int>* vec_clusterID      = new std::vector<int>();
    tInFile->SetBranchAddress("clusterID", &vec_clusterID);
    std::vector<int>* vec_clusterNum     = new std::vector<int>();
    tInFile->SetBranchAddress("clusterNum", &vec_clusterNum);

    // TFile *OutFile = new TFile(directory+RunNumber+"/Aligned/Clusterxy_"+hitOption+"_Eff6_5Selected.root", "RECREATE");
    TFile *OutFile = new TFile(directory+RunNumber+"/Eff2/Clusterxy_"+hitOption+"_"+EffOption+".root", "RECREATE");
    // Out Tree
    TTree* myTree = new TTree("Event", "beam data");

    int EventNum = -1;
    myTree->Branch("EventNum", &EventNum);
    int TotalClusters;
    myTree->Branch("TotClusterNum", &TotalClusters);
    std::vector<int>* vec_planeOut      = new std::vector<int>();
    myTree->Branch("planeID", &vec_planeOut);
    std::vector<int>* vec_ClusterNumOut      = new std::vector<int>();
    myTree->Branch("clusterNum", &vec_ClusterNumOut);
    std::vector<double>* vec_colOut      = new std::vector<double>();
    myTree->Branch("col", &vec_colOut);
    std::vector<double>* vec_rowOut      = new std::vector<double>();
    myTree->Branch("row", &vec_rowOut);
    
    std::vector<int>* vec_hitsNum      = new std::vector<int>();
    myTree->Branch("hitsNum", &vec_hitsNum);
    std::vector<int>* vec_hitsNumX      = new std::vector<int>(); 
    myTree->Branch("hitsNumX", &vec_hitsNumX);
    std::vector<int>* vec_hitsNumY      = new std::vector<int>();
    myTree->Branch("hitsNumY", &vec_hitsNumY);

    std::vector<double>* vec_xloc = new std::vector<double>();   
    myTree->Branch("xloc", &vec_xloc);
    std::vector<double>* vec_yloc = new std::vector<double>();
    myTree->Branch("yloc", &vec_yloc);

    std::vector<double>* vec_xglo = new std::vector<double>();   
    myTree->Branch("xglo", &vec_xglo);
    std::vector<double>* vec_yglo = new std::vector<double>();
    myTree->Branch("yglo", &vec_yglo);
    std::vector<double>* vec_zglo = new std::vector<double>();
    myTree->Branch("zglo", &vec_zglo);

    double aveCol, aveRow;
    double TotCol, TotRow;
    float x, y, xglo, yglo, zglo;
    std::vector<int> SelCluID;
    for (int m = 0; m < (int)tInFile->GetEntries(); m++){
      tInFile->GetEntry(m);
      if (m%100000 == 0) std::cout << "Processing " << m << "..." << std::endl;
      // if (m  >= 100) break;

      vec_rowOut->clear();
      vec_colOut->clear();
      vec_planeOut->clear();
      vec_ClusterNumOut->clear();
      vec_xloc->clear();
      vec_yloc->clear();
      vec_xglo->clear();
      vec_yglo->clear();
      vec_zglo->clear();
      vec_hitsNum->clear();
      vec_hitsNumX->clear();
      vec_hitsNumY->clear();
     
      EventNum = EventNumber;
      int accuPlaneHits = 0;
      TotalClusters = 0;
      for (int i = 0; i < NumOfChips; i++)
      {
        if (i == 2 && EffOption == "Eff5") continue;   //!!!!!!
        SelCluID.clear();
        std::vector<int> Selplane = findVectors(vec_plane, i);
        for (int j = Selplane[0]; j < Selplane[Selplane.size() - 1] + 1; j++)
        {
          SelCluID.push_back(vec_clusterID->at(j));
        }
        // for (int e = 0; e < SelCluID.size(); e++) std::cout << SelCluID[e] << std::endl;
        int planeClusters = vec_clusterNum->at(Selplane[0]);
        if (i != 2) {TotalClusters += planeClusters;}   //!!!!!
        for (int k = 0; k < planeClusters; k++)
        {
          aveRow = 0.;
          aveCol = 0.;
          TotRow = 0.;
          TotCol = 0.;
          std::vector<int> SelHitsInCluster = findVectors(&SelCluID, k);
          std::vector<int> rrow;
          std::vector<int> ccol;
          for (int l = 0; l < SelHitsInCluster.size(); l++)
          {
            TotRow += vec_row->at(accuPlaneHits+SelHitsInCluster.at(l));
            TotCol += vec_col->at(accuPlaneHits+SelHitsInCluster.at(l));
            rrow.push_back(vec_row->at(accuPlaneHits+SelHitsInCluster.at(l)));
            ccol.push_back(vec_col->at(accuPlaneHits+SelHitsInCluster.at(l)));
          }
          std::map<int, int> viDstX; std::map<int, int> viDstY;
          FindSamePosXY(ccol, viDstX);
          FindSamePosXY(rrow, viDstY);


          // std::map<int, int>::iterator it;
          // std::cout << "viDstX.size() = " << viDstX.size() << std::endl;
          // std::cout << "viDstY.size() = " << viDstY.size() << std::endl;

          aveRow = TotRow/SelHitsInCluster.size();
          aveCol = TotCol/SelHitsInCluster.size();
           
          // std::cout << "aveRow == " << aveRow << std::endl;
          // std::cout << "aveCol == " << aveCol << std::endl;
          vec_hitsNum->push_back(SelHitsInCluster.size());
          double  *PointXY = new double [2];
          pixelToPoint(aveRow, aveCol, PointXY);
          x = PointXY[0]*(1e-3);
          y = PointXY[1]*(1e-3);
          ROOT::Math::Transform3D m_transform = readConditions(filename, i);
          auto pGlobal = m_transform * ROOT::Math::XYZPoint(x, y, 0);
          xglo = pGlobal.x();
	        yglo = pGlobal.y();
	        zglo = pGlobal.z();
          delete[] PointXY;
          if (i == 2 && EffOption == "Eff6_5Selected") continue;    //!!!!!!!!!!!!!!
          vec_hitsNumX->push_back(viDstX.size());
          vec_hitsNumY->push_back(viDstY.size());

          vec_planeOut->push_back(i);
          vec_ClusterNumOut->push_back(planeClusters);

          vec_colOut->push_back(aveCol);
          vec_rowOut->push_back(aveRow);
          vec_xloc->push_back(x);
          vec_yloc->push_back(y);
          vec_xglo->push_back(xglo);
          vec_yglo->push_back(yglo);
          vec_zglo->push_back(zglo);
        }
        accuPlaneHits = accuPlaneHits + SelCluID.size();
      }

    // if (hitOption == "1hit" && vec_planeOut->size() == 6 && TotalClusters == 6) myTree->Fill();
    // if (hitOption == "1hit" && vec_planeOut->size() == 6) myTree->Fill();
    if (hitOption == "mhit" && TotalClusters == 6) myTree->Fill();
    else if (hitOption == "all" && TotalClusters <= 9) myTree->Fill();
    // else if (hitOption == "all") myTree->Fill();
    }
  myTree->Write();
  InFile->Close();
  OutFile->Close();
}
