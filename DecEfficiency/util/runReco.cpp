#include<iostream>
#include<fstream>
#include<vector>

#include "FindTimeStamp.h"
#include "FindAllHits.h"
#include "CreateCluster.h"
#include "CombineAllTracks.h"


int main(int argc, char **argv)
{
    TString directory = argv[1];
    TString RunNumber = argv[2];
    TString hitOption = argv[3];
    TString EffOption = argv[4];
    
    FindTimeStamp* findTimeStamp = new FindTimeStamp();
    findTimeStamp->initialize(directory, RunNumber);
    std::vector<std::vector<int>> Turnpoint = findTimeStamp->FindTurnPoint();
    findTimeStamp->FindTime(Turnpoint, directory, RunNumber);
    delete findTimeStamp;

    FindAllHits* findHits = new FindAllHits();
    findHits->FindHits(Turnpoint, directory, RunNumber, EffOption);
    delete findHits;
    
    CreateCluster* clustering = new CreateCluster();
    clustering->FormCluster(directory, RunNumber, hitOption, EffOption);
    delete clustering;

    CombineAllTracks* combinetracks = new CombineAllTracks();
    combinetracks->CombineCluster(directory, RunNumber, hitOption, EffOption);
    delete combinetracks;



}
