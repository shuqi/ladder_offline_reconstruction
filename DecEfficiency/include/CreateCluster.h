#ifndef _CreateCluster_H
#define _CreateCluster_H 1

#include <iostream>
#include <fstream>
#include <vector>

#include <TString.h>
#include <TSystem.h>
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

#include "Math/Point3D.h"
#include "Math/Vector3D.h"
#include "Math/Rotation3D.h"
#include "Math/Transform3D.h"
#include "Math/RotationZ.h"
#include "Math/RotationX.h"
#include "Math/RotationY.h"
#include "Math/Translation3D.h"
#include "Math/RotationZYX.h"

#include "IncludeFun.h"
#include "constants.h"

class CreateCluster{

protected:
  
public:

  CreateCluster();
  ~CreateCluster();
  
  void FormCluster(TString directory, TString RunNumber, TString hitOption, TString EffOption);

private:

};

ROOT::Math::Transform3D readConditions(TString filename, int i)
{
  // double setAlignment[6];
  std::ifstream fin(filename);
  std::string line_info;
  double input_result;
  std::vector<double> vectorString;
  if (fin)
  {
    while (getline (fin, line_info))
    {
      std::stringstream input(line_info);
      while(input>>input_result)
      {
        vectorString.push_back(input_result);
      }
    }
  }
  else
  {
    std::cout<<"no such file"<<std::endl;
  }

  // for (int k = 1; k < 7; k++)
  // {
  //   std::cout << "vectorString[i*7+1] = " << vectorString[i*7+k]  << std::endl;
  //   // setAlignment[k-1] = vectorString[i*7+k];
  // }
  ROOT::Math::Translation3D t(vectorString[i*7+1], vectorString[i*7+2], vectorString[i*7+3]);
  ROOT::Math::RotationZYX r(vectorString[i*7+6], vectorString[i*7+5], vectorString[i*7+4]);
  
  ROOT::Math::Transform3D m_transform = ROOT::Math::Transform3D(r, t);

  ROOT::Math::XYZPoint check;
  // m_transform.GetTranslation(check);
  // std::cout << check.x() << std::endl;
  // std::cout << check.y() << std::endl;
  // std::cout << check.z() << std::endl;

  fin.close();
  return m_transform;
}

int equals(int a, int b)
{
  return a == b;
}

void FindSamePosXY(std::vector<int> &src, std::map<int, int> &dst)
{
  for (int i = 0; i < src.size(); ++i)
  {
    int checkStr = src[i];
    int flag = 0;
    for (int j = 0; j < i; ++j)
    {
      if (equals(checkStr, src[j]))
      {
        flag = 1;
        break;
      }
    }
    if (!flag)
    {
      int count = 1;
      for (int j = i + 1; j < src.size(); ++j)
      {
        if (equals(checkStr, src[j]))
        {
          ++count; 
        }
      }
      dst.insert(std::pair<int, int>(checkStr, count));
    
    }
  
  }
 
}

void pixelToPoint(double row, double col, double Point[2])
{

  if (col <= 512) // 451 // 512 1023/2
  {
    Point[0] = -((512 - col)*25);
  }
  else
  {
    Point[0] = (col - 512)*25;
  }

  if (row  <= 256)  // 238 // 256
  {
    Point[1] = -((256 - row)*25);
  }
  else
  {
    Point[1] = (row - 256)*25;
  }
}

#endif
