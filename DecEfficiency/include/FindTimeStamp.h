#ifndef _FindTimeStamp_H
#define _FindTimeStamp_H 1

#include <iostream>
#include <fstream>
#include <vector>

#include <TString.h>
#include <TSystem.h>
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

#include "constants.h"

class FindTimeStamp{

protected:
  
   TFile *Infile;

    
    int maxLoop;
    int minLoop;
    int maxPosLoop;
    int minPosLoop;

public:

  FindTimeStamp();
  ~FindTimeStamp();
  
  bool initialize(TString directory, TString RunNumber);
  std::vector<std::vector<int>> FindTurnPoint();
  void FindTime(std::vector<std::vector<int>> vec_Findpos, TString directory, TString RunNumber);

private:
};

std::vector<int> FindZero(TFile *inputFile, int ith)
{
  std::vector<int> vec_Findpos;
  TString treeName = "HitsInfo_"+std::to_string(ith);
  TTree *t = (TTree*)inputFile->Get(treeName);
  int timeFPGA;
  t->SetBranchAddress("timeFPGA", &timeFPGA);
  int pretimeFPGA = 0;
  int curtimeFPGA = 0;
  int numth = 0;
  vec_Findpos.clear();
  for (int i = 0; i < (int)t->GetEntries(); i++)
  {
    t->GetEntry(i);
    pretimeFPGA = curtimeFPGA;
    curtimeFPGA = timeFPGA;
    if (pretimeFPGA > curtimeFPGA)
    {
      numth++;
      // if (nums > 1) break;
      std::cout << "pretimeFPGA > curtimeFPGA" << std::endl;
      std::cout << "position = " << i << std::endl;
      std::cout << "pretimeFPGA = " << pretimeFPGA << std::endl;
      std::cout << "curtimeFPGA = " << curtimeFPGA << std::endl;
      std::cout << "====================" << std::endl;
      vec_Findpos.push_back(i);
      
    }
  }
  return vec_Findpos;
}


#endif
