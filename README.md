## Decode binary file
```
cd scripts
root -l DeBinary.cpp
root -l removeSameTimeStamp.cpp
hadd Outputhit2_9.root Outputhit_2_9_Board06_remove2.root Outputhit_2_9_Board05_remove2.root Outputhit_2_9_Board04_remove2.root Outputhit_2_9_Board03_remove2.root Outputhit_2_9_Board02_remove2.root Outputhit_2_9_Board01_remove2.root

```
## Track Reconstruction 
### Running 
```
mkdir build
cd build
cmake ../
make
./runReco "/home/dell/ladder_Offline_Reconstruction/ladder_DESY_data/" "Run0638" "all"

```
### Function of code in src/

*  FindTimeStamp: Find matched FPGA time stamp  
*  FindAllHits: Find all hits with same FPGA time stamp  
*  CreateCluster:  Clustering  
*  CombineAllTracks: Combine all possible tracks  


## Track Fit 
### Running
```
cd TrackFitMinuit  
mkdir build  
cd build  
cmake ../  
./runFit "Run0614" 
```
You can find the output root file in the Output directory



