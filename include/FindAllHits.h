#ifndef _FindAllHits_H
#define _FindAllHits_H 1

#include <iostream>
#include <fstream>
#include <vector>

#include <TString.h>
#include <TSystem.h>
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

#include "IncludeFun.h"
#include "constants.h"

class FindAllHits{

protected:
  
public:

  FindAllHits();
  ~FindAllHits();
  
  void FindHits(std::vector<std::vector<int>> vec_Findpos, TString directory, TString RunNumber);

private:
};

int GetCorrectCol(int row, int col)
{
  int newCol;
  if ((row%4 == 0) || (row%4 == 3)){ newCol = col*2; }
  else { newCol = col*2+1; }
  return newCol;
}

int FindTime(TFile *inputFile, int target, std::vector<std::vector<int>*> outputTree, int chipID, int tag[11]){
  TString treeName = "HitsInfo_"+std::to_string(chipID);
  TTree *t = (TTree*)inputFile->Get(treeName);
  int row, col, timeFPGA, timeChip;
  int new_row, new_col;
  int mir_row, mir_col;
  int cchipID;
  t->SetBranchAddress("chipID", &cchipID);
  t->SetBranchAddress("row", &row);
  t->SetBranchAddress("col", &col);
  t->SetBranchAddress("timeFPGA", &timeFPGA);
  t->SetBranchAddress("timeChip", &timeChip);
  // std::cout << (int)t->GetEntries() << std::endl;
  // std::cout << "chipID = " << chipID << std::endl; std::cout << "tag = " << tag[chipID] << std::endl; std::cout << "=============" << std::endl;
  int FindHitsNum = 0;
  for (int i = tag[chipID]; i < (int)t->GetEntries(); i++)
  {
    t->GetEntry(i);
    
    if (passTime(target, timeFPGA, 0))
    {
      tag[chipID] = i;
      outputTree[0]->push_back(chipID);
      outputTree[1]->push_back(timeFPGA);
      outputTree[2]->push_back(timeChip);
      new_row = row/2;
      new_col = GetCorrectCol(row, col);
      
      if ((chipID == 0) || (chipID == 2))
      {
        mir_col = 1023 - new_col;
        mir_row = new_row;
      }
      else if ((chipID == 5) || (chipID == 7) || (chipID == 9))
      {
        mir_row = 511 - new_row;
        mir_col = new_col;
      }
      
      else if ((chipID == 6) || (chipID == 8) || (chipID == 10))
      {
        mir_row = 511 - new_row;
        mir_col = 1023 - new_col;
      }
      else
      {
        mir_row = new_row;
        mir_col = new_col;
      }

      // if ((chipID == 0) || (chipID == 2))
      // {
      //   mir_col = 1023 - new_col;
      //   mir_row = new_row;
      // }
      // else if ((chipID == 6) || (chipID == 8))
      // {
      //   mir_row = 511 - new_row;
      //   mir_col = new_col;
      // }
      
      // else if ((chipID == 5) || (chipID == 7) || (chipID == 9))
      // {
      //   mir_row = 511 - new_row;
      //   mir_col = 1023 - new_col;
      // }
      // else
      // {
      //   mir_row = new_row;
      //   mir_col = new_col;
      // }
      outputTree[3]->push_back(mir_row);
      outputTree[4]->push_back(mir_col);
      FindHitsNum++;
    }
    else if (passbreak(target, timeFPGA, 1)) {tag[chipID] = i; break;}
    else if (passcontinue(target, timeFPGA, 1)) {continue;}
  }
  return FindHitsNum;
}
#endif
